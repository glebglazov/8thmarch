let wizard;

var nav = (document.layers);
var tmr = null;
var spd = 50;
var x = 0;
var x_offset = 5;
var y = 0;
var y_offset = 15;


const wizardAnimations = ['Read', 'Write', 'Process', 'Think', 'Blink'];

let onChangeEvent = debounce(function() {
    wizardAnimate(wizardAnimations[Math.floor(Math.random() * wizardAnimations.length)]);
}, 500);

$( function() {
    let editor = CodeMirror.fromTextArea(document.getElementById('editor'), {
        mode: 'text/x-ruby',
        identUnit: 2,
        lineNumbers: true,
    });
    editor.on('change', onChangeEvent);

    $("form").submit(function(e) {

        e.preventDefault();

        let form = $(this);
        let url = form.attr('action');

        wizardAnimate('DoMagic1');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data, text_status, xhr)
            {
                if (xhr.status == 204) {
                    $('main').html("<img width='100%' src='/images/finish.jpg'>");
                    wizard.stopCurrent();
                    wizard.stop();
                    wizard.speak('Благодаря тебе, гендерные праздники спасены! За это ты получаешь следующую подсказку');
                    setTimeout(() => { wizard.hide() }, 6000 );
                } else
                {
                    $('#errors').html(data)
                }
            }
        });
    });

    clippy.load('Merlin', function(agent){
        wizard = agent;
        agent.show();
        agent.speak('...');
        agent.speak('Дорогая Яна. Прежде чем продолжить, помоги мне спасти гендерные праздники');
        agent.moveTo(700, 300);
    });
});

function wizardAnimate(animation) {
    wizard.stopCurrent();
    wizard.play(animation);
}

function debounce(func, wait, immediate) {
    let timeout;
    return function() {
        let context = this, args = arguments;
        let later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };

        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

if(nav) document.captureEvents(Event.MOUSEMOVE);
document.onmousemove = get_mouse;

function get_mouse(e)
{
    x = (nav) ? e.pageX : event.clientX+document.body.scrollLeft;
    y = (nav) ? e.pageY : event.clientY+document.body.scrollTop;
    x += x_offset;
    y += y_offset;
    beam(1);
}

function beam(n)
{
    if(n<5)
    {
        if(nav)
        {
            eval("document.div"+n+".top="+y);
            eval("document.div"+n+".left="+x);
            eval("document.div"+n+".visibility='visible'");
        } else {
            eval("div"+n+".style.top="+y);
            eval("div"+n+".style.left="+x);
            eval("div"+n+".style.visibility='visible'");
        }
        n++;
        tmr=setTimeout("beam("+n+")",spd);
    }  else  {
        clearTimeout(tmr);
        fade(4);
    }
}

function fade(n)
{
    if(n>0)
    {
        if(nav)eval("document.div"+n+".visibility='hidden'");
        else eval("div"+n+".style.visibility='hidden'");
        n--;
        tmr=setTimeout("fade("+n+")",spd);
    }
    else clearTimeout(tmr);
}


//FIREFLOWERRRRRR

(function() {
    var root = document.querySelector('.fire-flower');
    var box = root.querySelector('.fire-flower__wrapper');
    var head = box.querySelector('.fire-flower__head');
    var body = document.querySelector('body');

    var center = {
        x: body.offsetWidth,
        y: body.offsetWidth / 2
    };
    var mousePos;
    var percent = {
        height: 0,
        width: 0
    };
    var rotate = 0;
    var screen = {
        height: root.offsetHeight,
        width: root.offsetWidth
    };

    const MAXDEGS = {
        x: (36) / 400,
        // y: (11.25) / 260
        y: (18) / 400
    };

    /**
     * Event bindings and methods
     */

    root.addEventListener('click', onMouseMove);
    body.addEventListener('mousemove', onMouseMove);
    root.addEventListener('mouseleave', onMouseLeave);

    function onMouseLeave(event) {
        head.style.transform = 'rotate(0)';
    }

    function onMouseMove(event) {
        event = event || window.event; // IE-ism

        mousePos = calculateMousePosition(event);

        rotate = calculateRotation();
        let transform = `rotateX(${rotate.x * -1}deg)`;
        transform += `rotateY(${rotate.y}deg)`;
        transform += `rotateZ(${rotate.y / -8}deg)`;

        head.style.transform = transform;
    }

    /**
     * General methods
     */

    function calculateRotation() {
        var x = (mousePos.y - center.y) * MAXDEGS.y;
        var y = (mousePos.x - center.x) * MAXDEGS.x;

        return {
            x: x,
            y: (y * 1) * 0.40
        };
    }

    function calculateMousePosition(event) {
        // If pageX/Y aren't available and clientX/Y are,
        // calculate pageX/Y - logic taken from jQuery.
        // (This is to support old IE)
        if (event.pageX == null && event.clientX != null) {
            let eventDoc = (event.target && event.target.ownerDocument) || document;
            let doc = eventDoc.documentElement;
            let body = eventDoc.body;

            event.pageX =
                event.clientX +
                ((doc && doc.scrollLeft) || (body && body.scrollLeft) || 0) -
                ((doc && doc.clientLeft) || (body && body.clientLeft) || 0);
            event.pageY =
                event.clientY +
                ((doc && doc.scrollTop) || (body && body.scrollTop) || 0) -
                ((doc && doc.clientTop) || (body && body.clientTop) || 0);
        }

        return {
            x: event.pageX,
            y: event.pageY
        };
    }

})();
