# -*- coding: utf-8 -*-

RSpec::Core::Reporter.new(RSpec.configuration).tap do |reporter|
  RSpec.describe do
    let(:male_name) do
      %w[
        Ашот
        Айрат
        Огюст
        Фархад
        Яков
      ].sample
    end

    let(:female_name) { 'Яна' }

    let(:february23_date) { Time.new(2019, 2, 23) }
    let(:march8_date) { Time.new(2019, 3, 8) }

    let(:date) { march8_date }

    let(:formatted_february23_date) { '23 февраля' }
    let(:formatted_march8_date) { '8 марта' }

    let(:formatted_date) { date == march8_date ? formatted_march8_date : formatted_february23_date }
    let(:adjective) { date == march8_date ? 'Дорогая' : 'Дорогой' }

    let(:expected_result) { "#{adjective} #{subject.name}, поздравляем с #{formatted_date}" }

    subject { GenderSpecificHolidaysGreeter.new(name, date) }

    context do
      let(:name) { male_name }
      let(:date) { february23_date }

      it do
        expect(subject.call).to eq(expected_result)
      end
    end

    context do
      let(:name) { female_name }

      it do
        expect(subject.call).to eq(expected_result)
      end
    end
  end.run(reporter)
end
