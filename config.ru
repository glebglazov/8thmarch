# -*- coding: utf-8 -*-

require 'pry'
require 'sinatra'
require 'rspec'
require 'json'

Encoding.default_external = Encoding::UTF_8

class App < Sinatra::Base
  before { cache_control :public, :must_revalidate, :max_age => 60 }

  get '/' do
    @code = File.read('./lib/content.rb').force_encoding(Encoding::UTF_8)

    erb :index
  end

  post '/upload' do
    if code = params['code']
      @code = code.force_encoding(Encoding::UTF_8)

      prepared_rspec_tests = File.read('./lib/tests.rb')

      eval_code = [@code, prepared_rspec_tests].map { |c| c.force_encoding(Encoding::UTF_8) }.join("\n")

      begin
        reporter = Timeout.timeout(20) { Module.new.module_eval(eval_code) }

        if reporter.failed_examples.count != 0
          tests_li = reporter.failed_examples.map { |example| "<li>#{example.display_exception}</li>" }.join("\n")
          return "<ul>#{tests_li}</ul>"
        else
          status 204
        end
      rescue Exception => e
        @failed_test = e.message.force_encoding(Encoding::UTF_8)

        return @failed_test
      end
    else
      erb :index
    end
  end
end

run App
